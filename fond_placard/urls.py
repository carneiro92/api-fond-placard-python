from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.contrib import admin
from .apps.recipes.viewsets import RecipesViewset
from .apps.ingredients.viewsets import IngredientsViewset
from rest_framework.authtoken.views import obtain_auth_token


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'ingredients', IngredientsViewset)
router.register(r'recipes', RecipesViewset)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    #path('', include(router.urls)),
    path('api-auth/', include(router.urls)),
    #api-token-auth used to login in the api and recieve a token
    path('api-token-auth/', obtain_auth_token, name = 'api-token-auth'),
    path('admin/', admin.site.urls),
]
