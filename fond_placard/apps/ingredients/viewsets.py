from rest_framework import viewsets
from .models import Ingredients
from . import serializer


class IngredientsViewset(viewsets.ModelViewSet):
    queryset = Ingredients.objects.all()
    serializer_class = serializer.IngredientsSerializer