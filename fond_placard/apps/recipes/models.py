from django.db import models



class Recipes(models.Model):
    name = models.CharField(max_length=120)
    category = models.CharField(max_length=120)
    picture = models.CharField(max_length=50)
    score = models.IntegerField()
    ingredients = models.ManyToManyField(Ingredients)

    