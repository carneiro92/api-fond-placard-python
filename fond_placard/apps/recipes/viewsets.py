from rest_framework import viewsets
from . import models
from . import serializer


class RecipesViewset(viewsets.ModelViewSet):
    queryset = models.Recipes.objects.all()
    serializer_class = serializer.RecipesSerializer
    models.Recipes.objects.all()